# Build scripts for Lorry Controller using BuildStream

This project contains element definitions for building:

* Lorry
* Lorry Controller
* various Python modules they require
* various version control systems and conversion tools they require
* lighttpd
* cmdtest/yarn and OpenRC, required to run self-tests
* A container image (OCI or Docker v1.2 format) with Lorry Controller,
  Lorry, and the other required services

It requires Buildstream 1.4 and bst-external 0.20, and pulls many
other element definitions from the Freedesktop-SDK.

## Building a container image

### OCI - for podman or Docker registry

Run:

* `bst build lorry-oci.bst`
* `bst checkout -f --tar lorry-oci.bst lorry-oci.tar`

### Docker v1.2 - for Docker CLI

Run:

* `bst build lorry-docker.bst`
* `bst checkout -f --tar lorry-docker.bst lorry-docker.tar`

## Using a container image

The container provides web (http) and Secure Shell (ssh) services on
the default ports, which will need to be mapped to specific ports on
the host.

It requires site-specific configuration to be provided as a volume
mounted at `/etc/lc-site`:

1. Start by copying the `conf/site` directory from this repository.
2. Edit these files to define how to access the Downstream Host and
   CONFGIT:

   * `lorry-controller/lorry.conf`
   * `lorry-controller/webapp.conf`
   * `ssh/known_hosts`

3. Add the SSH public keys for the Lorry Controller administrators to
   `ssh/authorized_keys`.
4. Put the number of concurrent mirror jobs you want to run in
   `lorry-controller/minion-concurrency`.

In case you are using Gitea or GitLab as the Downstream Host, you will
need to put the API access token in the environment variable
`lorry_gitea_access_token` or `lorry_gitlab_private_token`
respectively.

You will need to:

1. Create a user and group for Lorry on the host.
2. Create a base directory for the Lorry 'working area', owned by
   that user and group.  This is a cache that should persist across
   container upgrades.

In case you are using a local filesystem as the Downstream Host, you
will also need to:

1. Create the base directory for mirrors, owned by that user and
   group.
2. Bind-mount that directory into the container.

### podman

Run:

* `podman load -i lorry-oci.tar`
* `podman tag` *image-id* `lorry`
* `podman create --name lorry -t -p` *ssh-port*`:22 -p` *http-port*`:80 \`<BR>
  `-v /`*working-area-dir*`:/srv/lorry-working-area:rw \`<BR>
  `-v /`*site-conf-dir*`:/etc/lc-site:ro lorry`
* `podman start lorry`

### Docker

Run:

* `docker load -i lorry-docker.tar`
* `docker create --name lorry -t -p` *ssh-port*`:22 -p`  *http-port*`:80 \`<BR>
  `--stop-signal=SIGRTMIN+3 --tmpfs /run -v /sys/fs/cgroup:/sys/fs/cgroup:ro \`<BR>
  `-v /`*working-area-dir*`:/srv/lorry-working-area:rw \`<BR>
  `-v /`*site-conf-dir*`:/etc/lc-site:ro lorry`
* `docker start lorry`

When the container boots, it will generate an SSH key pair for Lorry,
and the public key will be visible on its web server at
`/lorry-ssh-pubkey`.  This key needs to be added to Lorry's user
account on the Downstream Host.

### Status page

The status page for Lorry Controller is the front page on the
container's public web service.

### Administration interface

To access the administration interface, you need to set up an SSH
tunnel to the container:

`ssh -N -p` *ssh-port* `-L 12765:localhost:12765 lorry@`*hostname*

This command produces no output, and should be left running until you
have finished.  The administration interface will then be visible at
<http://localhost:12765/1.0/status-html>.
